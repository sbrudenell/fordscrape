import pathlib

import pytest
from fordscrape.state import State


@pytest.fixture
def state(tmp_path: pathlib.Path) -> State:
    return State(tmp_path / "state")


async def test_initialize_default(state: State) -> None:
    await state.initialize(make="testmake", model="testmodel", market="testmarket")
    assert await state.get_search_params() == {
        "make": "testmake",
        "model": "testmodel",
        "market": "testmarket",
    }


async def test_initialize_with_search_params(state: State) -> None:
    await state.initialize(
        make="testmake", model="testmodel", market="testmarket", engine="testengine"
    )
    assert await state.get_search_params() == {
        "make": "testmake",
        "model": "testmodel",
        "market": "testmarket",
        "engine": "testengine",
    }
